import './globals.css'
import {AuthProvider} from "@/providers/auth/AuthProvider";
export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      {/*
        <head /> will contain the components returned by the nearest parent
        head.tsx. Find out more at https://beta.nextjs.org/docs/api-reference/file-conventions/head
      */}
      <head><title>text</title></head>
      <body>
      <main className="w-screen h-screen">
          <section className="py-10 bg-gray-50 sm:py-16 lg:py-24 h-full">
          <AuthProvider>
            {children}
          </AuthProvider>
          </section>
      </main>
      </body>
    </html>
  )
}
