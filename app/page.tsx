import {withLogin} from "@/lib/withLogin";
import Link from "next/link";

async function ListComponent() {
  const result = await getUsersData();
  const list = result?.data as {id: string; name: string; phone: string; email: string}[];
  return (
      <div className="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
        <div className="flex flex-row justify-between items-center">
          <h2 className="text-3xl font-bold leading-tight text-black sm:text-4xl lg:text-5xl">User List</h2>
          <Link href="/create"><span className="inline-flex items-center justify-center w-full px-6 py-4 text-base font-semibold text-white transition-all duration-200 bg-blue-600 border border-transparent rounded-md focus:outline-none hover:bg-blue-700 focus:bg-blue-700">Create</span></Link>
        </div>
        <div className="flex flex-col">
          <div className="overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div className="inline-block min-w-full py-2 sm:px-6 lg:px-8">
              <div className="overflow-hidden">
                <table className="min-w-full text-left text-sm">
                  <thead className="border-b font-medium dark:border-neutral-500">
                  <tr>
                    <th scope="col" className="px-6 py-4">#</th>
                    <th scope="col" className="px-6 py-4">Name</th>
                    <th scope="col" className="px-6 py-4">Mail</th>
                    <th scope="col" className="px-6 py-4">Phone</th>
                  </tr>
                  </thead>
                  <tbody>
                  {list.map((user, index) => (
                      <tr className="border-b dark:border-neutral-500" key={user.id}>
                        <td className="whitespace-nowrap px-6 py-4 font-medium">{index+1}</td>
                        <td className="whitespace-nowrap px-6 py-4">{user.email}</td>
                        <td className="whitespace-nowrap px-6 py-4">{user.phone}</td>
                        <td className="whitespace-nowrap px-6 py-4">{user.name}</td>
                      </tr>
                  ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
  );
}
function Home() {
   return (
       <>
         {/* @ts-expect-error Server Component */}
         <ListComponent />
       </>
  )
}

async function getUsersData() {
  const res = await fetch('http://localhost:3000/api/view?limit=10&offset=2', {
    method: 'GET', // *GET, POST, PUT, DELETE, etc.
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json'
    },
  });
  if (!res.ok) {
    throw new Error('Failed to fetch data');
  }

  return res.json();
}

export default withLogin(Home)
