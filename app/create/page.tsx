"use client"
import {useForm} from "react-hook-form";
import {validateEmail, validatePasswordFormat, validatePhoneNumber} from "@/services/validation";
import {createUser} from "@/services/createUserService";
import {useRouter} from "next/navigation";
import {useState} from "react";

interface FormValues {
    username: string
    password: string
    phone: string
    email: string
}

const inputClass = 'p-2 h-12 mt-1 block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm';
export default function CreateUser() {
    const router = useRouter();
    const {
        register,
        formState: {errors},
        handleSubmit,
    } = useForm<FormValues>({mode: "onChange"});
    const [msgErrorServer, setMsg] = useState<string>('')
    const onSubmit = handleSubmit(async ({username, password, email, phone}) => {
        // You should handle login logic with username, password and remember form data
        const result = await createUser('api/create', {
            name: username,
            password,
            email,
            phone,
        })
        debugger
        if (result?.success) {
            return router.push('/');
        }
        setMsg('Error, please try again');
    })
    return (
        <div className="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
            <div className="max-w-2xl mx-auto text-center">
                <h2 className="text-3xl font-bold leading-tight text-black sm:text-4xl lg:text-5xl">Create
                    new User</h2>
                <p className="max-w-xl mx-auto mt-4 text-base leading-relaxed text-gray-600">Please enter your input</p>
            </div>

            <div className="relative max-w-md mx-auto mt-8 md:mt-16">
                <form onSubmit={onSubmit}>
                    <div className="overflow-hidden shadow sm:rounded-md">
                        <div className="bg-white px-4 py-5 sm:p-6">
                            <div className="grid grid-cols-1 gap-6">
                                <div className="grid grid-cols-1 gap-2">
                                    <label htmlFor="username"
                                           className="block text-sm font-medium text-gray-700">User
                                        name</label>
                                    <input
                                        id='username'
                                        type="text"
                                        placeholder="User name"
                                        {...register('username', {
                                            required: {value: true, message: 'User name is required'},
                                            minLength: {
                                                value: 2,
                                                message: 'User name cannot be less than 3 character',
                                            },
                                            maxLength: {
                                                value: 1000,
                                                message: 'User name cannot be more than 1000 character',
                                            }
                                        })}
                                        className={inputClass}
                                    />
                                    {errors.username && (
                                        <span className="text-sm text-red-800">{errors.username.message}</span>
                                    )}
                                </div>

                                <div>
                                    <label htmlFor="password"
                                           className="block text-sm font-medium text-gray-700">Password</label>
                                    <input
                                        id='password'
                                        type="password"
                                        placeholder="Password"
                                        {...register('password', {
                                            required: {value: true, message: 'Password is required'},
                                            validate: {
                                                password: value => validatePasswordFormat(value)
                                            }
                                        })}
                                        className={inputClass}/>
                                    {errors?.password?.message && (
                                        <span className="text-sm text-red-800">{errors.password.message}</span>
                                    )}
                                    {errors?.password?.type === 'password' && (
                                        <span className="text-sm text-red-800">Should have at least a number, a special character, and be more than 8 characters long</span>
                                    )}
                                </div>

                                <div>
                                    <label htmlFor="phone"
                                           className="block text-sm font-medium text-gray-700">Number
                                        phone</label>
                                    <input
                                        id='phone'
                                        type="text"
                                        placeholder="Phone"
                                        {...register('phone', {
                                            required: {value: true, message: 'Phone is required'},
                                            validate: {
                                                phone: value => validatePhoneNumber(value)
                                            }
                                        })}
                                        className={inputClass}
                                    />
                                    {errors?.phone?.message && (
                                        <span className="text-sm text-red-800">{errors.phone.message}</span>
                                    )}
                                    {errors?.phone?.type === 'phone' && (
                                        <span className="text-sm text-red-800">Telephone number is invalid</span>
                                    )}
                                </div>
                                <div>
                                    <label htmlFor="email"
                                           className="block text-sm font-medium text-gray-700">Email
                                        address</label>
                                    <input
                                        id='email'
                                        type="text"
                                        placeholder="Email"
                                        {...register('email', {
                                            required: {value: true, message: 'Email is required'},
                                            validate: {
                                                email: value => validateEmail(value)
                                            }
                                        })}
                                        className={inputClass}/>
                                    {errors.email && errors.email.message && (
                                        <span className="text-sm text-red-800">{errors.email.message}</span>
                                    )}
                                    {errors.email && errors.email.type === 'email' && (
                                        <span className="text-sm text-red-800">Email is invalid</span>
                                    )}
                                </div>
                            </div>
                        </div>
                        <p className="py-4 px-6 text-red-800">{msgErrorServer}</p>
                        <div className="bg-gray-50 px-4 py-3 text-right sm:px-6">
                            <button type="submit"
                                    className="inline-flex justify-center rounded-md border border-transparent bg-indigo-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2">Save
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    );
}
