import React from 'react';
import Login from "@/components/Login";
import { LoginProvider } from "@/providers/login/LoginProvider";

export default function LoginPage() {
    return (
        <LoginProvider>
            <Login />
        </LoginProvider>
    );
}
