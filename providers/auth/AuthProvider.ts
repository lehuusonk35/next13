'use client';
import createFastContext from "@/providers/wapperContext";

const { Provider: AuthProvider, useStore: authStore } = createFastContext({
    username: "",
});

export {
    AuthProvider,
    authStore,
}
