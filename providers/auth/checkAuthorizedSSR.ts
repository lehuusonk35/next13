import {cookies} from "next/headers";
import { redirect } from 'next/navigation';
export const checkAuthorizedSSR = (callback: () => Promise<void>) => {
    return async () => {
        try {
            const cookieStore = cookies();
            const username = cookieStore.get('username');
            if (!username) {
                return redirect('/login');
            }

            return await callback();
        } catch (e) {
            console.error(e);
        }
    };
};
