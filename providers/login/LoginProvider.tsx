'use client';
import createFastContext from "@/providers/wapperContext";

const { Provider: LoginProvider, useStore: loginStore } = createFastContext({
    username: "",
    password: "",
    errorMsg: ""
});

export {
    LoginProvider,
    loginStore,
}
