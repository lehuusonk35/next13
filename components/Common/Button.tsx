import React, {ButtonHTMLAttributes} from 'react';
interface TButton extends ButtonHTMLAttributes<HTMLButtonElement> {
    onClick: () => void
}
const Button = ({ onClick, ...rest}: TButton) => {
    const handleOnClick = (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      event.preventDefault();
      onClick?.();
    }
    return (
        <button onClick={handleOnClick} {...rest}
                className="inline-flex items-center justify-center w-full px-4 py-4 text-base font-semibold text-white transition-all duration-200 bg-blue-600 border border-transparent rounded-md focus:outline-none hover:bg-blue-700 focus:bg-blue-700">
            Log in
        </button>
    )
}

export default Button;
