import React from 'react';

const LoginTitle = () => {

    return (
        <div className="max-w-2xl mx-auto text-center">
            <h2 className="text-3xl font-bold leading-tight text-black sm:text-4xl lg:text-5xl">Welcome
                Back!</h2>
            <p className="max-w-xl mx-auto mt-4 text-base leading-relaxed text-gray-600">Login to your
                account</p>
        </div>
    )
}

export default LoginTitle;
