'use client';
import React from 'react';
import LoginForm from "@/components/Login/LoginForm";
import LoginTitle from "@/components/Login/LoginTitle";
import {loginStore} from "@/providers/login/LoginProvider";
import {loginUser} from "@/services/loginServices";
import { useRouter } from 'next/navigation';
import {authStore} from "@/providers/auth/AuthProvider";

const Login = () => {
    const router = useRouter();
    const [{ username, password }, setStore] = loginStore((store) => store);
    const [ , setAuthValue] = authStore((store) => store['username']);
    const onSubmitForm = async () => {
       const { auth, errorDetails } = await loginUser('/api/auth', {username, password});
       if (auth) {
           setAuthValue({['username']: username})
           document.cookie = 'username='+username
           return router.push('/');
       }
       setStore({['errorMsg']: errorDetails})
    };

    return (
        <div className="px-4 mx-auto max-w-7xl sm:px-6 lg:px-8">
            <LoginTitle />
            <LoginForm onSubmit={onSubmitForm} />
        </div>
    )
}

export default Login;
