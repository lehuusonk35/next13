'use client';
import React, {ChangeEvent, useCallback} from 'react';
import {loginStore} from "@/providers/login/LoginProvider";
const MAP_LABEL = {
    username: 'User name',
    password: 'Password'
}
const PLACEHOLDER = {
    username: 'Input user name',
    password: 'Input password'
}

const TextInput = ({value, type}: {value: 'username' | 'password'; type: string}) => {
    const [fieldValue, setStore] = loginStore((store) => store[value]);
    const handleOnChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
        setStore({ [value]: e.target.value })
    }, [value])
    return (
        <div>
            <label htmlFor={value} className="text-base font-medium text-gray-900">{MAP_LABEL[value]}</label>
            <div className="mt-2.5 relative text-gray-400 focus-within:text-gray-600">
                <div
                    className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                    <svg className="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none"
                         viewBox="0 0 24 24" stroke="currentColor">
                        <path strokeLinecap="round" strokeLinejoin="round"
                              strokeWidth="2"
                              d="M16 12a4 4 0 10-8 0 4 4 0 008 0zm0 0v1.5a2.5 2.5 0 005 0V12a9 9 0 10-9 9m4.5-1.206a8.959 8.959 0 01-4.5 1.207"/>
                    </svg>
                </div>

                <input
                    type={type}
                    id={value}
                    value={fieldValue}
                    onChange={handleOnChange}
                    placeholder={PLACEHOLDER[value]}
                    className="block w-full py-4 pl-10 pr-4 text-black placeholder-gray-500 transition-all duration-200 bg-white border border-gray-200 rounded-md focus:outline-none focus:border-blue-600 caret-blue-600"
                />
            </div>
        </div>
    )
}
export default TextInput;
