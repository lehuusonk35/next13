'use client';

import React from 'react';
import TextInput from "@/components/Login/TextInput";
import Button from "@/components/Common/Button";
import {loginStore} from "@/providers/login/LoginProvider";
type TLoginForm = {
    onSubmit: () => void
}
const LoginForm = ({ onSubmit }: TLoginForm) => {
    const [errorMsg] = loginStore((store) => store['errorMsg']);
    return (
        <div className="relative max-w-md mx-auto mt-8 md:mt-16">
            <div className="overflow-hidden bg-white rounded-md shadow-md">
                <div className="px-4 py-6 sm:px-8 sm:py-7">
                    <form>
                        <div className="space-y-5">
                            <TextInput value='username' type='text' />
                            <TextInput value='password' type='password' />
                            <p className="text-red-700">{errorMsg}</p>
                            <div>
                                <Button onClick={onSubmit} />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default LoginForm;
