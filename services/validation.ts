const emailRegex = /^[a-zA-Z0-9][\w\-.+_%!#$%&'*+-/=?^_`{}~|]*@[\w\.\-]+\.[A-Za-z0-9]{2,}$/i;
const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[\w\W]{8,}$/;
const phoneNumberRegex = /^(0|[+84])([0-9]{9})$/i;

function validateEmail(value: string): boolean {
    return emailRegex.test(value);
}

function validatePasswordFormat(value: string): boolean {
    return passwordRegex.test(value);
}

function validatePhoneNumber(value: string): boolean {
    return phoneNumberRegex.test(value);
}

export {
    validateEmail,
    validatePasswordFormat,
    validatePhoneNumber,
}
