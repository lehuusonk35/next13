import React, {ComponentType} from 'react';
type Base = {};
import {cookies} from "next/headers";
import { redirect } from 'next/navigation';
export const withLogin = <TProps extends Base>(Component: ComponentType<TProps>) => {
    return (props: TProps) => {
        const cookieStore = cookies();
        const username = cookieStore.get('username');
        console.log(username, 'cookieStore');
        if (!username) {
            return redirect('/login');
        }
        return <Component {...props} />;
    };
};
